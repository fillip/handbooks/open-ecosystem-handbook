# Open Ecosystems Handbook

This handbook is a guide on what open ecosystems are, the benefits they bring, and how to effectively operate within open ecosystems, specifically with a view towards collaborative technology projects.

## What are open ecosystems?

To understand open ecosystems we must first explore the definition of ecosystems from an organizational, and fundamentally economic and business perspective. Ecosystems are an essential part of the most financially successful companies in the world, and are comprised of two value vectors: horizontal and vertical. The horizontal value vector is a stream for consolidating a range of customers across sectors, where-as the vertical vector provide depth to dominate the touch points across customer journeys.

For this to be effective, ecosystems are built through partnerships, distributing specific value offerings by providing incentives that enable innovation to happen faster, with reduced risk. 

Open ecosystems can be conceived around the same needs for distributed innovation and responsive inventiveness. However, they differ from the traditional economic ecosystem by focusing predominently on their own customers, and enabling implicit partnerships through open interfaces. This is often achieved through standardization and cooperative development. One of the most successful mechanisms for open ecosystems is the Web. Built on common, and living standards, the Web enables both content provides to publish semantically meaningful information, and enables browser vendors, data brokers, and search engines to access that information for meaningful productization. 

